import java.util.Objects;

public class Track {
    private String name, genre;
    private int year;

    Track() {}

    Track (String name, String genre, int year) {
        this.name = name;
        this.genre = genre;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public String getGenre() {
        return genre;
    }

    public int getYear() {
        return year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Track track = (Track) o;
        return year == track.year &&
                Objects.equals(name, track.name) &&
                Objects.equals(genre, track.genre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, genre, year);
    }
}
