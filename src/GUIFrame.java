import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUIFrame extends JFrame {

    public GUIFrame(DataBase dataBase) {
        super("2");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel contentPanel = new JPanel();

        DefaultListModel<String> dlm = new DefaultListModel<String>();
        for (Track i : dataBase.getTracks()) {
            dlm.add(dlm.size(), dataBase.getTrackInfo(i.getName()));
        }

        JTextField queryField = new JTextField(15);
        JButton searchButton = new JButton("Поиск");


        String[] cases = {"Поиск треков по артистам", "Поиск треков по альбомам", "Поиск треков по компиляциям", "Поиск альбомов по артистам"};
        JComboBox<String> caseBox = new JComboBox<>(cases);

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String result = "";
                switch ((String) caseBox.getSelectedItem()) {
                    case "Поиск треков по артистам": {
                        result = dataBase.findTracksByArtist(queryField.getText());
                        break;
                    }
                    case "Поиск треков по альбомам": {
                        result = dataBase.findTracksFromAlbum(queryField.getText());
                        break;
                    }
                    case "Поиск треков по компиляциям": {
                        result = dataBase.findTracksFromCompilation(queryField.getText());
                        break;
                    }
                    case "Поиск альбомов по артистам": {
                        result = dataBase.findAlbumsByArtist(queryField.getText());
                        break;
                    }
                }

                dlm.removeAllElements();
                dlm.add(dlm.size(), result);
                validate();
            }
        });

        JButton resetButton = new JButton("Сброс");
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dlm.removeAllElements();
                for (Track i : dataBase.getTracks()) {
                    dlm.add(dlm.size(), dataBase.getTrackInfo(i.getName()));
                }
                validate();
            }
        });

        JList<String> tracklist = new JList<String>(dlm);
        JScrollPane tracklistSP = new JScrollPane(tracklist);
        tracklistSP.setPreferredSize(new Dimension(400, 200));
        tracklistSP.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        contentPanel.add(tracklistSP);
        contentPanel.add(caseBox);
        contentPanel.add(queryField);
        contentPanel.add(searchButton);
        contentPanel.add(resetButton);

        setContentPane(contentPanel);
        setSize(580, 350);
        setResizable(false);
        setVisible(true);
    }

    public static void main(String[] args) {
        //new GUIFrame();
    }
}
