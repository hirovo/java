

import javafx.util.Pair;

import java.util.HashMap;
import java.util.HashSet;

public class DataBase {
    private HashSet<Artist> artists = new HashSet<Artist>();
    private HashSet<Track> tracks = new HashSet<Track>();
    private HashSet<MusicCollection> musicCollections = new HashSet<MusicCollection>();
    private HashMap<String, HashSet<String>> genres = new HashMap<String, HashSet<String>>();

    private HashSet<Pair<Artist, Track>> artistTrack = new HashSet<Pair<Artist, Track>>();
    private HashSet<Pair<Artist, MusicCollection>> artistMusicCollection = new HashSet<Pair<Artist, MusicCollection>>();
    private HashSet<Pair<Track, MusicCollection>> trackMusicCollection = new HashSet<Pair<Track, MusicCollection>>();
    private HashSet<Pair<Track, String>> trackGenre = new HashSet<Pair<Track, String>>();

    public DataBase() {
    }

    public void addGenre(String genreName) {
        genres.put(genreName, new HashSet<String>());
    }

    public void addSubgenre(String genreName, String subgenreName) {
        genres.get(genreName).add(subgenreName);
    }

    public void addTrack(String artistName, String trackName, String genreName, int year) {
        Artist artist = new Artist(artistName);
        Track track = new Track(trackName, genreName, year);
        artists.add(artist);
        tracks.add(track);
        artistTrack.add(new Pair<Artist, Track>(artist, track));
        trackGenre.add(new Pair<Track, String>(track, genreName));
    }

    public void addTrackInAlbum(String artistName, String trackName, String genreName, int year, String albumName) {
        Artist artist = new Artist(artistName);
        Track track = new Track(trackName, genreName, year);
        MusicCollection album = new Album(albumName);
        artists.add(artist);
        tracks.add(track);
        musicCollections.add(album);

        artistTrack.add(new Pair<Artist, Track>(artist, track));
        trackGenre.add(new Pair<Track, String>(track, genreName));
        artistMusicCollection.add(new Pair<Artist, MusicCollection>(artist, album));
        trackMusicCollection.add(new Pair<Track, MusicCollection>(track, album));
    }

    public void addTrackInCompilation(String artistName, String trackName, String genreName, int year, String compilationName) {
        Artist artist = new Artist(artistName);
        Track track = new Track(trackName, genreName, year);
        MusicCollection compilation = new Compilation(compilationName);
        artists.add(artist);
        tracks.add(track);
        musicCollections.add(compilation);

        artistTrack.add(new Pair<Artist, Track>(artist, track));
        trackGenre.add(new Pair<Track, String>(track, genreName));
        artistMusicCollection.add(new Pair<Artist, MusicCollection>(artist, compilation));
        trackMusicCollection.add(new Pair<Track, MusicCollection>(track, compilation));
    }

    public String findTracksByArtist(String artistName) {
        StringBuilder answer = new StringBuilder();
        for (Pair<Artist, Track> pair : artistTrack) {
            if (pair.getKey().getName().equals(artistName)) {
                if (answer.length() != 0) {
                    answer.append("; ");
                }
                answer.append(pair.getValue().getName());
            }
        }
        if (answer.length() == 0) {
            answer = new StringBuilder("There are no tracks by " + artistName + ".");
        } else {
            answer.append(".");
        }
        return answer.toString();
    }

    public String findTracksFromAlbum(String albumName) {
        StringBuilder answer = new StringBuilder();
        for (Pair<Track, MusicCollection> pair : trackMusicCollection) {
            if (pair.getValue() instanceof Album && pair.getValue().getName().equals(albumName)) {
                if (answer.length() != 0) {
                    answer.append("; ");
                }
                answer.append(pair.getKey().getName());
            }
        }
        if (answer.length() == 0) {
            answer = new StringBuilder("There are no tracks from the " + albumName + " album.");
        } else {
            answer.append(".");
        }
        return answer.toString();
    }

    public String findTracksFromCompilation(String compilationName) {
        StringBuilder answer = new StringBuilder();
        for (Pair<Track, MusicCollection> pair : trackMusicCollection) {
            if (pair.getValue() instanceof Compilation && pair.getValue().getName().equals(compilationName)) {
                if (answer.length() != 0) {
                    answer.append("; ");
                }
                answer.append(pair.getKey().getName());
            }
        }
        if (answer.length() == 0) {
            answer = new StringBuilder("There are no tracks from the " + compilationName + " compilation.");
        } else {
            answer.append(".");
        }
        return answer.toString();
    }

    public String findTracksOfGenre(String genreName) {
        StringBuilder answer = new StringBuilder();
        for (Pair<Track, String> pair : trackGenre) {
            if (pair.getValue().equals(genreName) || genres.get(genreName).contains(pair.getValue())) {
                if (answer.length() != 0) {
                    answer.append("; ");
                }
                answer.append(pair.getKey().getName());
            }
        }
        if (answer.length() == 0) {
            answer = new StringBuilder("There are no tracks of a " + genreName + " genre.");
        } else {
            answer.append(".");
        }
        return answer.toString();
    }

    public String findTracksOfYear(int year) {
        StringBuilder answer = new StringBuilder();
        for (Track track : tracks) {
            if (track.getYear() == year) {
                if (answer.length() != 0) {
                    answer.append("; ");
                }
                answer.append(track.getName());
            }
        }
        if (answer.length() == 0) {
            answer = new StringBuilder("There are no tracks of a " + Integer.toString(year) + " year.");
        } else {
            answer.append(".");
        }
        return answer.toString();
    }

    public String findAlbumsByArtist(String artistName) {
        StringBuilder answer = new StringBuilder();
        for (Pair<Artist, MusicCollection> pair : artistMusicCollection) {
            if (pair.getValue() instanceof Album && pair.getKey().getName().equals(artistName)) {
                if (answer.length() != 0) {
                    answer.append("; ");
                }
                answer.append(pair.getValue().getName());
            }
        }
        if (answer.length() == 0) {
            answer = new StringBuilder("There are no albums by " + artistName + ".");
        } else {
            answer.append(".");
        }
        return answer.toString();
    }

    public String findCompilationsByArtist(String artistName) {
        StringBuilder answer = new StringBuilder();
        for (Pair<Artist, MusicCollection> pair : artistMusicCollection) {
            if (pair.getValue() instanceof Compilation && pair.getKey().getName().equals(artistName)) {
                if (answer.length() != 0) {
                    answer.append("; ");
                }
                answer.append(pair.getValue().getName());
            }
        }
        if (answer.length() == 0) {
            answer = new StringBuilder("There are no compilations by " + artistName + ".");
        } else {
            answer.append(".");
        }
        return answer.toString();
    }

    public String findArtistsOfTrack(String trackName) {
        StringBuilder answer = new StringBuilder();
        for (Pair<Artist, Track> pair : artistTrack) {
            if (pair.getValue().getName().equals(trackName)) {
                if (answer.length() != 0) {
                    answer.append("; ");
                }
                answer.append(pair.getKey().getName());
            }
        }
        if (answer.length() == 0) {
            answer = new StringBuilder("There are no artists of " + trackName + ".");
        }
        return answer.toString();
    }

    public String findArtistsOfAlbum(String albumName) {
        StringBuilder answer = new StringBuilder();
        for (Pair<Artist, MusicCollection> pair : artistMusicCollection) {
            if (pair.getValue() instanceof Album && pair.getValue().getName().equals(albumName)) {
                if (answer.length() != 0) {
                    answer.append("; ");
                }
                answer.append(pair.getKey().getName());
            }
        }
        if (answer.length() == 0) {
            answer = new StringBuilder("There are no artists of " + albumName + ".");
        } else {
            answer.append(".");
        }
        return answer.toString();
    }

    public String findArtistsOfCompilation(String compilationName) {
        StringBuilder answer = new StringBuilder();
        for (Pair<Artist, MusicCollection> pair : artistMusicCollection) {
            if (pair.getValue() instanceof Compilation && pair.getValue().getName().equals(compilationName)) {
                if (answer.length() != 0) {
                    answer.append("; ");
                }
                answer.append(pair.getKey().getName());
            }
        }
        if (answer.length() == 0) {
            answer = new StringBuilder("There are no artists of " + compilationName + ".");
        } else {
            answer.append(".");
        }
        return answer.toString();
    }

    public HashSet<Artist> getArtists() {
        return artists;
    }

    public String trackToString(Track track) {
        /*String result = track.getName()+" by ";
        for (Track )*/
        return "bitch lasagna";
    }

    public String findAlbumOfTrack(String trackName) {
        StringBuilder answer = new StringBuilder();
        for (Pair<Track, MusicCollection> pair : trackMusicCollection) {
            if (pair.getKey().getName().equals(trackName)) {
                if (answer.length() != 0) {
                    answer.append("; ");
                }
                answer.append(pair.getValue().getName());
            }
        }
        if (answer.length() == 0) {
            answer = new StringBuilder("There are no Album of " + trackName + ".");
        } else {
            answer.append(" ");
        }
        return answer.toString();
    }

    public String findGenresOfTrack(String trackName) {
        StringBuilder answer = new StringBuilder();
        for (Pair<Track, String> pair : trackGenre) {
            if (pair.getKey().getName().equals(trackName)) {
                if (answer.length() != 0) {
                    answer.append("; ");
                }
                answer.append(pair.getValue());
            }
        }
        if (answer.length() == 0) {
            answer = new StringBuilder("There are no genre of " + trackName + ".");
        } else {
            answer.append(" ");
        }
        return answer.toString();
    }

    public String getTrackInfo(String trackName) {
        StringBuilder answer = new StringBuilder();
        answer.append(trackName+" by ");
        answer.append(findArtistsOfTrack(trackName) + "; ");
        answer.append(findAlbumOfTrack(trackName) + "; ");
        answer.append(findGenresOfTrack(trackName));
        return answer.toString();
    }

    public String getAllTracks() {
        StringBuilder answer = new StringBuilder();
        for (Track track : tracks) {
            answer.append(getTrackInfo(track.getName()));
            answer.append("/n");
        }
        return answer.toString();
    }

    public HashSet<Track> getTracks() {
        return this.tracks;
    }
}
